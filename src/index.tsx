import * as React from 'react';
import * as ReactDOM from 'react-dom';

// Binding window object 
declare global{
  interface Window {
    __REDUX__DECTOOLS_EXTENSION_COMPOSE__: any
  }
}

import App from './App';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(
    <App />
  ,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
