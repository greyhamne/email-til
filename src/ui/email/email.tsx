import * as React from 'react';
import './email.scss'

const Email = (props:any) => {
    


    return (
        <div className="email">
            <div className="email__header">
                <a className="email__online-version" href="#">View online version</a>
                <img src="https://www.scottishpower.co.uk/content/images/emails/smart/sp-logo.png" alt="" className="email__logo"/>
            </div>

            <hr className="email__split"/>

            <div className="email__body">
                <p className="email__account-number">Account Number: {props.data.GROUPACCOUNTNUMBER}</p>
                <div className="email__intro">
                    <div className="email__intro-col">
                        <h1 className="email__intro-title">Extend your fixed prices for an {props.data.EXTRA_MONTHS}</h1>
                        <h2 className="email__intro-subTitle">The same fixed price, for even longer. It's simple.</h2>
                    </div>

                    <div className="email__intro-col">
                        <img src="https://www.scottishpower.co.uk/content/images/emails/400493/extend_18_months_banner.jpg" alt=""/>
                    </div>
                </div>

                <p className="email__copy">Hello {props.data.TITLE_SURNAME},</p>

                <p className="email__copy">
                    We wanted to let you know that now your fixed tariff has ended, you've automatically been moved to our {props.data.CURRENT_PRODUCT_DESCRIPTION} tariff. Full details of this change including important information about your new tariff can be found in <a href="http://campaign.scottishpower-online.co.uk/c/19SSfucmK71a1dcExJJvREER"><strong>MyMessages</strong></a>.
                </p>

                <p className="email__copy">
                    And you can now extend your new fixed prices for an extra {props.data.EXTRA_MONTHS}. By taking action now, you'll be protected against increasing energy prices <strong>until {props.data.END_DATE_FULL}*</strong>.
                </p>

                <p className="email__copy">
                    There's no change to how you manage your energy or how you pay for it. It's simple.
                </p>

                <div className="email__extending">
                   <div className="email__extending-container">
                        <h2 className="email__extending-title">Extending your fixed prices is quick and easy</h2>
                        <p className="email__extending-copy">
                            Please read the <a href="http://campaign.scottishpower-online.co.uk/c/19SSfFII3cloSpiOjPQYXTAO"><strong>Tariff  Information</strong></a> and what this means for you before choosing this new {props.data.NEW_TARIFF_NAME} tariff.
                        </p>
                        <button className="email__extending-cta">Extend now </button>
                        <p className="email__extending-copy">
                            Alternatively you can view our full range of tariffs in your <a href="http://campaign.scottishpower-online.co.uk/c/19SSg2LoFmZSANv7S25VansI">Online Account</a>.
                        </p>
                   </div>
                </div>

                 <p className="email__copy email__copy--thanks">Thanks for being a ScottishPower customer.</p>

            </div>

            <div className="email__footer-banner">
                <div className="email__body email__body--green-banner">
                    <p className="email__copy email__copy--white email__copy--remove-margin">
                       <a href="https://www.scottishpower.co.uk/"><strong>www.scottishpower.co.uk</strong></a>.
                    </p>
                </div> 
            </div>

            <div className="email__body">
                <div className="email__uswitch-row">
                    <div className="email__uswitch-col">
                        <img src="#" alt=""/>
                    </div>

                    <div className="email__uswitch-col">
                        <img src="" alt=""/>
                    </div>

                    <div className="email__uswitch-col">
                        <img src="" alt=""/>
                    </div>
                </div>

                <p className="email__copy">
                    <a href="https://www.scottishpower.co.uk/pdf/tariffs/2018/T_and_Cs_FFL_Feb_2021.pdf" ><strong>* Full tariff Terms and Conditions</strong></a> apply. Your bill/Direct Debit amount may vary depending on your energy consumption. 
                </p>

                <p className="email__copy">
                    Data for this mailing was generated and correct on 9th June 2018 to communicate relevant tariff information to ScottishPower customers. If you have recently moved to a new ScottishPower tariff or different supplier, please disregard this email.
                </p>

                <p className="email__copy">
                    <strong>Why was I sent this email?</strong><br/>As a valued energy customer, you will occasionally receive messages about the service we provide you. This is just to keep you up to date and to help you get the most from your online energy account.
                </p>

                <p className="email__copy">
                    ScottishPower Energy Retail Limited <br/>
                    Registered Office: 320 St. Vincent Street, Glasgow G2 5AD<br/>
                    Registered in Scotland No. 190287, Vat No. GB 659 3720 08
                </p>

                <p className="email__copy">
                    We understand the importance of keeping your personal details safe. <br/>To find out more, visit <a href="http://campaign.scottishpower-online.co.uk/c/19SSgpO5hxEmjbHrqekRmRkC" target="_blank"><strong>www.getsafeonline.org</strong></a>
                </p>

                <p className="email__copy">
                    Please consider the environment before printing this email.
                </p>

                <p className="email__copy">
                    If you have received this message in error, please notify the sender and immediately delete this message and any attachment hereto and/or copy hereof, as such message contains confidential information intended solely for the individual or entity to whom it is addressed. The use or disclosure of such information to third parties is prohibited by law and may give rise to civil or criminal liability.
                </p>

                <p className="email__copy">
                    The views presented in this message are solely those of the author(s) and do not necessarily represent the opinion of Iberdrola, S.A. or any company of its group. Neither Iberdrola, S.A. nor any company of its group guarantees the integrity, security or proper receipt of this message. Likewise, neither Iberdrola, S.A. nor any company of its group accepts any liability whatsoever for any possible damages arising from, or in connection with, data interception, software viruses or manipulation by third parties.
                </p>
            </div>
        </div>
    )
}

export default Email;

