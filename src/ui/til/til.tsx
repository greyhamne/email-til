import * as React from 'react';
import './til.scss'
import moment from 'moment-es6'

const Til = (props: any) => {
	// const date = props.data.CURRENT_GAS_TARIFF_END_ON;
	// const testDate = new Date ();
	// console.log(testDate);


    return (
		<div className="til">
			<header className="til__header">
				<div className="til__header-logo-container">
					<img className="til__header-logo" src="https://www.scottishpower.co.uk/content/images/header-footer/logo.jpg" alt=""/>
				</div>
			</header>

			<div className="til__wrapper">
				<div className="til__intro">
					<h1 className="til__intro-title">Tariff Information and what this means for you</h1>
					<p className="til__intro-copy">You'll find below your estimated annual costs on our {props.data.NEW_TARIFF_NAME} tariff, plus the tariff information and Principal Terms for both your current tariff, and this new tariff, in an easy to compare format.</p>
					<p className="til__intro-copy"><strong>As a reminder, the only difference between your current tariff and our {props.data.NEW_TARIFF_NAME} tariff are the end date and tariff name, giving you an extra {props.data.EXTRA_MONTHS} at exactly the same fixed prices as your current tariff.</strong></p>
				</div>
			</div>

			<div className="til__wrapper">
				<h2>Your estimated annual costs</h2>
				<div className="til__row">
					<div className="til__col til__col--header" />

					<div className="til__col til__col--header">
						<p>Your energy use over the last 12 months</p>
					</div>

					<div className="til__col til__col--header">
						<p>{props.data.NEW_TARIFF_NAME} tariff</p>
					</div>
				</div>

				<div className="til__row">
					<div className="til__col">
						<p>Gas</p>
					</div>

					<div className="til__col">
						<p>{props.data.CURRENT_GAS_CONSUMPTION}</p>
					</div>

					<div className="til__col">
						<p>{props.data.CURRENT_GAS_VALUE}</p>
					</div>
				</div>

				<div className="til__row">
					<div className="til__col">
						<p>Electricity</p>
					</div>

					<div className="til__col">
						<p>{props.data.CURRENT_ELEC_CONSUMPTION}</p>
					</div>

					<div className="til__col">
						<p>{props.data.CURRENT_ELEC_VALUE}</p>
					</div>
				</div>

				<div className="til__row">
					<div className="til__col">
						<p>Annual Total</p>
					</div>

					<div className="til__col til__col--large">
						<p>{props.data.CURRENT_TOTAL_VALUE}</p>
					</div>
				</div>

				<div className="til__row">
					<div className="til__col til__col--full ">
						<p>This estimate is based on your previous 12 months' energy use (from actual meter readings or best estimates), relevant tariff prices, any applicable discounts and includes VAT. It does not include your current balance. Your estimated annual costs will vary if your energy use.</p>
					</div>
				</div>

			</div>

			<div className="til__wrapper">
				<div className="til__section-header">
					<h2 className="til__section-title">
						Gas tariff information for your current tariff and our {props.data.NEW_TARIFF_NAME} tariff
					</h2>
					<p className="til__section-copy">
						The Tariff Information Labels (TILs) below summarise all the key details about your current gas tariff and our {props.data.NEW_TARIFF_NAME} tariff. Some charges and/or discounts may not apply to your account, depending on how you have chosen to manage your energy. Please see our <a href="https://www.scottishpower.co.uk/pdf/Standard_General_Terms_and_Conditions_for_domestic_customers.pdf">Terms and Conditions</a> for details.
					</p>
				</div>

				<div className="til__row">
					<div className="til__col til__col--header">Tariff Information</div>
					<div className="til__col til__col--header">About your current gas tariff</div>
					<div className="til__col til__col--header">About our {props.data.NEW_TARIFF_NAME} electricity tariff</div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Supplier</strong></p></div>
					<div className="til__col"><p>ScottishPower</p></div>
					<div className="til__col"><p>ScottishPower</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Tariff name</strong></p></div>
					<div className="til__col"><p>{props.data.CURRENT_GAS_PRODUCT_DESCRIPTION}</p></div>
					<div className="til__col"><p>{props.data.NEW_TARIFF_NAME}</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Tariff type</strong></p></div>
					<div className="til__col"><p>{props.data.TARIFF_TYPE}</p></div>
					<div className="til__col"><p>{props.data.TARIFF_TYPE}</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Payment method</strong></p></div>
					<div className="til__col">
						<p>
							{props.data.CURRENT_GAS_PAYMENT_METHOD === "BDM" ? "Monthly Direct Debit" : null}
							{props.data.CURRENT_GAS_PAYMENT_METHOD === "RCQ" ? "Quarterly Cash" : null}
							{props.data.CURRENT_GAS_PAYMENT_METHOD === "RDQ" ? "Quarterly Direct Debit" : null}
							{props.data.CURRENT_GAS_PAYMENT_METHOD === "BMW" ? "Pay As You Go" : null}
						</p>
					</div>
					<div className="til__col">
						<p>
							{props.data.CURRENT_GAS_PAYMENT_METHOD === "BDM" ? "Monthly Direct Debit" : null}
							{props.data.CURRENT_GAS_PAYMENT_METHOD === "RCQ" ? "Quarterly Cash" : null}
							{props.data.CURRENT_GAS_PAYMENT_METHOD === "RDQ" ? "Quarterly Direct Debit" : null}
							{props.data.CURRENT_GAS_PAYMENT_METHOD === "BMW" ? "Pay As You Go" : null}
						</p>
					</div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Unit rate - All/Day</strong></p></div>
					<div className="til__col"><p>{props.data.CURRENT_GAS_PRICE_KEY_VALUE_DAY}</p></div>
					<div className="til__col"><p>{props.data.CURRENT_GAS_PRICE_KEY_VALUE_DAY}</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Standing Charge</strong></p></div>
					<div className="til__col"><p>{props.data.CURRENT_GAS_PRICE_KEY_VALUE_SERVICE}</p></div>
					<div className="til__col"><p>{props.data.CURRENT_GAS_PRICE_KEY_VALUE_SERVICE}</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Tariff ends on</strong></p></div>
					
					<div className="til__col"><p> {moment(props.data.CURRENT_GAS_TARIFF_END_ON).format('do MMMM YYYY')}</p></div>
					<div className="til__col"><p>{moment(props.data.CURRENT_GAS_TARIFF_END_ON).format('do MMMM YYYY')}</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Price guaranteed until</strong></p></div>
					<div className="til__col"><p>{moment(props.data.CURRENT_GAS_PRICE_GUARANTEED_UNTIL).format('do MMMM YYYY')}</p></div>
					<div className="til__col"><p>{moment(props.data.CURRENT_GAS_PRICE_GUARANTEED_UNTIL).format('do MMMM YYYY')}</p></div>
				</div>

				

				<div className="til__row">
					<div className="til__col"><p><strong>Exit fee</strong>(If you switch  supplier more than 49 days before the tariff end date)</p></div>
					<div className="til__col"><p>{props.data.CURRENT_GAS_EXIT_FEES}</p></div>
					<div className="til__col"><p>{props.data.CURRENT_GAS_EXIT_FEES}</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Discounts and additional charges</strong></p></div>
					<div className="til__col"><p>Additional charges may apply - see our <a href="#">Frequently asked questions</a> for details</p></div>
					<div className="til__col"><p>Additional charges may apply - see our <a href="#">Frequently asked questions</a> for details</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Additional products or services included</strong></p></div>
					<div className="til__col"><p>{props.data.CT_ELEC_ADDITIONAL_PRODUCTS_OR_SERVICES_INCLUDED}</p></div>
					<div className="til__col"><p>{props.data.CT_ELEC_ADDITIONAL_PRODUCTS_OR_SERVICES_INCLUDED}</p></div>
				</div>

				{props.data.SERVICE_TYPE === "G" 
					? 
						<div className="til__row">
							<div className="til__col til__col--full">
								<div>
									<p>All prices include VAT at the appropriate rate which could change. Your bill or statement will show any VAT charged as a separate item.</p>
									<p>Prices correct at {moment(props.data.DATA_SUPPLY_DATE_FULL).format('do MMMM YYYY')}</p>
								</div>
							</div>
						</div>
					: null
				}

				<p>TEST ONLY - REMOVE ON LIVE</p>
				<p>PES Gas: {props.data.PES_GAS}</p>
				<p>Meter Register: {props.data.METER_REGISTER}</p>
				<p>Gas Payment Method: {props.data.CURRENT_GAS_PAYMENT_METHOD}</p>

				



			</div>

			<div className="til__wrapper">
				<div className="til__section-header">
					<h2 className="til__section-title">Electricity tariff information for your current tariff and our {props.data.new_tariff_name} tariff</h2>
					<p className="til__section-copy">
						The Tariff Information Labels (TILs) below summarise all the key details about your current electricity tariff and our new_tariff_name tariff. Some charges and/or discounts may not apply to your account, depending on how you have chosen to manage your energy. Please see our <a href="https://www.scottishpower.co.uk/pdf/Standard_General_Terms_and_Conditions_for_domestic_customers.pdf">Terms and Conditions</a> for details.
					</p>
				</div>

				<div className="til__row">
					<div className="til__col til__col--header"><p>Tariff Information</p></div>
					<div className="til__col til__col--header"><p>About your current electricity tariff</p></div>
					<div className="til__col til__col--header"><p>About our {props.data.new_tariff_name} tariff</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Supplier</strong></p></div>
					<div className="til__col"><p>ScottishPower</p></div>
					<div className="til__col"><p>ScottishPower</p></div>
				</div>
			
				<div className="til__row">
					<div className="til__col"><p><strong>Tariff name</strong></p></div>
					<div className="til__col"><p>{props.data.CURRENT_ELEC_PRODUCT_DESCRIPTION}</p></div>
					<div className="til__col"><p>{props.data.NEW_TARIFF_NAME}</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Tariff type</strong></p></div>
					<div className="til__col"><p>{props.data.TARIFF_TYPE}</p></div>
					<div className="til__col"><p>{props.data.TARIFF_TYPE}</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Payment method</strong></p></div>
					<div className="til__col">
						<p>
							{props.data.CURRENT_ELEC_PAYMENT_METHOD === "BDM" ? "Monthly Direct Debit" : null}
							{props.data.CURRENT_ELEC_PAYMENT_METHOD === "RCQ" ? "Quarterly Cash" : null}
							{props.data.CURRENT_ELEC_PAYMENT_METHOD === "RDQ" ? "Quarterly Direct Debit" : null}
							{props.data.CURRENT_ELEC_PAYMENT_METHOD === "BMW" ? "Pay As You Go" : null}
						</p>
					</div>
					<div className="til__col">
						<p>
							{props.data.CURRENT_ELEC_PAYMENT_METHOD === "BDM" ? "Monthly Direct Debit" : null}
							{props.data.CURRENT_ELEC_PAYMENT_METHOD === "RCQ" ? "Quarterly Cash" : null}
							{props.data.CURRENT_ELEC_PAYMENT_METHOD === "RDQ" ? "Quarterly Direct Debit" : null}
							{props.data.CURRENT_ELEC_PAYMENT_METHOD === "BMW" ? "Pay As You Go" : null}
						</p>
					</div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Unit rate - All/Day</strong></p></div>
					<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_DAY}</p></div>
					<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_DAY}</p></div>
				</div>

				{
					props.data.CURRENT_ELEC_PRICE_KEY_VALUE_NIGHT ? 
						<div className="til__row">
							<div className="til__col"><p><strong>Unit rate - Night</strong></p></div>
							<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_NIGHT}</p></div>
							<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_NIGHT}</p></div>
						</div>
					: null
				}
			
				{
					props.data.CURRENT_ELEC_PRICE_KEY_VALUE_PEAK ? 
						<div className="til__row">
							<div className="til__col"><p><strong>Unit rate - Peak</strong></p></div>
							<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_PEAK}</p></div>
							<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_PEAK}</p></div>
						</div>
					: null
				}

				{
					props.data.CURRENT_ELEC_PRICE_KEY_VALUE_OFF_PEAK ? 
						<div className="til__row">
							<div className="til__col"><p><strong>Unit rate - Off-Peak</strong></p></div>
							<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_OFF_PEAK}</p></div>
							<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_OFF_PEAK}</p></div>
						</div>
					: null
				}

				
				{
					props.data.CURRENT_ELEC_PRICE_KEY_VALUE_CONTROL ?
						<div className="til__row">
							<div className="til__col"><p><strong>Unit rate - Control</strong></p></div>
							<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_CONTROL}</p></div>
							<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_CONTROL}</p></div>
						</div>
					: null
				}

				<div className="til__row">
					<div className="til__col"><p><strong>Standing Charge</strong></p></div>
					<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_SERVICE}</p></div>
					<div className="til__col"><p>{props.data.CURRENT_ELEC_PRICE_KEY_VALUE_SERVICE}</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Tariff ends on</strong></p></div>
					<div className="til__col"><p>{moment(props.data.CURRENT_ELEC_TARIFF_END_ON).format('do MMMM YYYY')}</p></div>
					<div className="til__col"><p>{moment(props.data.NEW_ELEC_TARIFF_END_DATE).format('do MMMM YYYY')}</p></div>

					
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Price guaranteed until</strong></p></div>
					<div className="til__col"><p>{moment(props.data.CURRENT_ELEC_PRICE_GUARANTEED_UNTIL).format('do MMMM YYYY')}</p></div>
					<div className="til__col"><p>{moment(props.data.NEW_ELEC_TARIFF_END_DATE).format('do MMMM YYYY')}</p></div>
					
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Exit fee</strong> (If you switch supplier more than 49 days before the tariff end date)</p></div>
					<div className="til__col"><p>{props.data.CURRENT_ELEC_EXIT_FEES}</p></div>
					<div className="til__col"><p>{props.data.CURRENT_ELEC_EXIT_FEES}</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Discounts and additional charges</strong></p></div>
					<div className="til__col"><p>Additional charges may apply - see <a href="#FAQAdditionalCharges" title="Frequently asked questions">Frequently asked questions</a> for details</p></div>
					<div className="til__col"><p>Additional charges may apply - see <a href="#FAQAdditionalCharges" title="Frequently asked questions">Frequently asked questions</a> for details</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Additional products or services included</strong></p></div>
					<div className="til__col"><p>{props.data.CT_ELEC_ADDITIONAL_PRODUCTS_OR_SERVICES_INCLUDED}</p></div>
					<div className="til__col"><p>{props.data.CT_ELEC_ADDITIONAL_PRODUCTS_OR_SERVICES_INCLUDED}</p></div>
				</div>

					{props.data.SERVICE_TYPE === "DF" ||  props.data.SERVICE_TYPE === "E"
					? 
					<div className="til__row til__row--removeBg">
						<div className="til__col til__col--full">
							<p>
								All prices include VAT at the appropriate rate which could change. Your bill or statement will show any VAT charged as a separate item.
							</p>

							<p>
								Prices correct at {moment(props.data.DATA_SUPPLY_DATE_FULL).format('do MMMM YYYY')}
							</p>
						
						</div>
					</div>
					: null
				}

				<p>TEST ONLY - REMOVE ON LIVE</p>
				<p>PES Elec: {props.data.PES_ELEC}</p>
				<p>Meter Register: {props.data.METER_REGISTER}</p>
				<p>Elec Payment Method: {props.data.CURRENT_ELEC_PAYMENT_METHOD}</p>
				
			</div>

			<div className="til__wrapper">
				<div className="til__section-header">
					<h2 className="til__section-title">Frequently Asked Questions</h2>
				</div>

				<div className="til__row til__row--removeBg">
					<div className="til__col til__col--full">
						<h3 className="til__faqTitle">What is a kWh (kilowatt-hour)?</h3>
						<p>It’s a measure of the amount of energy used in one hour. For example, if you have a 1,000 watt heater and you keep it on for one hour, you’ll use 1kWh.</p>
					
						<h3 className="til__faqTitle">What additional charges or additional products or services may be applicable?</h3>
						<p>We charge additional fees for specific jobs, such as some metering jobs, or where we have to follow up debt. If these charges become applicable to you we will notify you.</p><br/>
						<p>From time to time we may have optional bundles or additional products or services available. You can find out more information on charges and additional optional bundles available by calling our Customer Service team free on <strong> 0800 027 0072</strong>.</p>
					</div>
				</div>
			</div>

			<div className="til__wrapper">
				<div className="til__section-header">
					<h2 className="til__section-title">Comparison of Principal Terms</h2>
					<p className="til__section-copy">This table compares the Principal Terms of your current {props.data.CURRENT_PRODUCT_DESCRIPTION} tariff and our {props.data.NEW_TARIFF_NAME} tariff.</p>
				</div>

				<div className="til__row">
					<div className="til__col til__col--header" />
					<div className="til__col til__col--header">{props.data.CURRENT_GAS_PRODUCT_DESCRIPTION} tariff</div>
					<div className="til__col til__col--header">{props.data.NEW_TARIFF_NAME} tariff</div>
				</div>

				<div className="til__row">
					<div className="til__col"><p><strong>Price information</strong></p></div>
					<div className="til__col"><p>Your prices are fixed until {moment(props.data.CURRENT_ELEC_TARIFF_END_ON).format('do MMMM YYYY')}. Your Direct Debit and/or bill amount may vary depending on your gas and/or electricity usage.</p></div>
					<div className="til__col"><p>Your prices are fixed until {moment(props.data.NEW_ELEC_TARIFF_END_DATE).format('do MMMM YYYY')}. Your Direct Debit and/or bill amount may vary depending on your gas and/or electricity usage.</p></div>
				</div>
				
				 
				
				<div className="til__row">
					<div className="til__col"> <p><strong>Ending your agreement</strong></p></div>
					<div className="til__col til__col--large">
						<p>Your agreement will end if:</p>
						<ul>
							<li>You choose another ScottishPower tariff</li>
							<li>You switch to another supplier</li>
							<li>You move house. You must give us at least two working days' notice or you will be liable for the supply until the new occupier enters into an energy supply contract or two working days after you give us notice</li>
						</ul>
						<p>Or we can end the agreement by giving you 42 calendar days' prior notice. This tariff doesn't have any exit fees.</p>
					</div>
				</div>

				<div className="til__row">
					<div className="til__col"> <p><strong>At the end of your tariff</strong></p></div>
					<div className="til__col til__col--large"><p>We'll write to you before the end of this tariff to let you know what your options are, including details of the tariff we will automatically move you to if you don't take action.</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"> <p><strong>If you fail to make a payment</strong></p></div>
					<div className="til__col til__col--large"><p>If you fail to make a payment when it’s due, we can make changes to your payment method, which may result in an increase in your prices. If you pay by Monthly Direct Debit, you will stay on the same tariff, but your unit rates will increase: your gas by 0.395p per kWh, and each electricity rate by 1.696p per kWh (inclusive of VAT).</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"> <p><strong>Green Deal</strong></p></div>
					<div className="til__col til__col--large"><p>If your property is subject to a Green Deal Plan, we will collect the Green Deal payments from you. Green Deal charges are not included in the prices for this tariff.</p></div>
				</div>

				<div className="til__row">
					<div className="til__col"> <p><strong>Full tariff Terms and Conditions</strong></p></div>
					<div className="til__col"><p><a href="#">{props.data.current_product_description} tariff Terms and Conditions</a> and ScottishPower's <a href="#">General Terms and Conditions for Domestic Customers</a> apply.</p></div>
					<div className="til__col"><p><a href="#">{props.data.new_tariff_name }tariff Terms and Conditions</a> and ScottishPower's <a href="#">General Terms and Conditions for Domestic Customers </a> apply.</p></div>
				</div>

				{
					props.data.CURRENT_ELEC_PAYMENT_METHOD === "BDM" || props.data.CURRENT_GAS_PAYMENT_METHOD ==="BDM" ?
						<div className="til__row">
							<div className="til__col til__col--full">
								<p><strong>Your Direct Debit</strong></p> 
								<br/>
								<p>Your Direct Debit amount may vary if your prices or energy use changes. We’ll review this annually, and also when we receive your meter readings. We’ll let you know of any changes to your payments in advance.</p>
							</div>
						</div>
					: null
				}
			</div>

			<div className="til__wrapper">
				<div className="til__choose">
					<h2 className="til__choose-title">Extending your fixed prices is quick and easy</h2>
					<p>All you need to do is click below to extend your prices to 31st January 2021 on our Fix for Longer January 2021.</p>
					<a className="til__intro-cta" href="https://www.scottishpower.co.uk/account/login">Choose my new tariff</a>
				</div>
			</div>
		</div>
          
		
    )
}

export default Til;