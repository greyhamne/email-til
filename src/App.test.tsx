import * as React from 'react';

import App from './App';

import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';

// tslint:disable-next-line:no-any
(Enzyme as any).configure({ adapter: new Adapter() })
const wrapper = Enzyme.shallow (<App />)  

describe('<App />', () => {
    it('should contain a container div', () => {
        expect(wrapper.find('.container').exists()).toBe(true);
    })

    it('matches the snapshot', () => {
        expect(toJson(wrapper)).toMatchSnapshot()
    })
})
