import * as React from 'react';
import Email from './ui/email/email';
import Til from './ui/til/til';
import sampleData from './sampleData/sample'

import './assets/main.scss'

class App extends React.Component<any> {


  public render() {
    // Create an empty array we will push results to
    const customerResults = [];
    const data = sampleData;

    // console.log(sampleData);

    for (const key in data) {
      if(data.hasOwnProperty(key)) {
        customerResults.push({
          id: key,
          data: data[key]
        });
      }
    }

    console.log(customerResults);

    return (
      <div className="container">
        {customerResults.slice(101, 120).map(element => (
          <div key={element.id}>
            <Email data={element.data} />
            <Til data={element.data}/>
          </div>
        ))}
      </div>
    );    
  }
}

export default App as any;
