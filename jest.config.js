module.exports = {
    verbose: true,
    testEnvironment: 'node',
    projects: [{
        runner: 'jest-runner',
        testURL: "http://localhost/",
    

      }],
      "moduleNameMapper": {
        "^.+\\.(css|less|scss)$": "identity-obj-proxy"
      },

    "roots": [
      "<rootDir>/src"
    ],
    "transform": {
      "^.+\\.tsx?$": "ts-jest"
    },
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
    "moduleFileExtensions": [
      "ts",
      "tsx",
      "js",
      "jsx",
      "json",
      "node"
    ],
  }